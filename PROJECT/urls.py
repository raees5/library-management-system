"""PROJECT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

# from APP.views forlder importing all files of views
from APP.views import author, book, subscriber, subscription, user, list_subscription


router = routers.SimpleRouter()
router.register('author', author.AuthorViewSet)
router.register('book', book.BookViewSet)
router.register('subscriber', subscriber.SubscriberViewSet)
router.register('subscription', subscription.SubscriptionViewSet)
# router.register('subscriberslist', subscription.SubscriberListView)
router.register('user', user.UserViewSet)



urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('list/', list_subscription.ListSubscriptionView.as_view()),
]

