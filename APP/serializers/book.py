from rest_framework_json_api import serializers
# from PROJECT.APP.models.author import Author
from ..models.book import Book
from .author import AuthorSerializer

class BookSerializer(serializers.ModelSerializer):

        included_serializers = {
                'author': AuthorSerializer
        }

        class Meta:
                model = Book
                fields = ('title', 'description', 'count', 'subscription_cost', 'topic', 'author')