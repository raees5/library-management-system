from rest_framework_json_api import serializers
from ..models.subscriber import Subscriber
from .user import UserSerializer


class SubscriberSerializer(serializers.ModelSerializer):

        included_serializers = {
                'user': UserSerializer
        }

        # users = serializers.StringRelatedField(many=True)

        class Meta:
                model = Subscriber
                fields = ('user', 'address', 'phone')