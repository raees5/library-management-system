from rest_framework_json_api import serializers
# from PROJECT.APP.models.author import Author
from ..models.author import Author
from .user import UserSerializer

class AuthorSerializer(serializers.ModelSerializer):

        included_serializers = {
                'user': UserSerializer
        }
        
        class Meta:
                model = Author
                fields = ('user',  'name', 'address')
                