from rest_framework_json_api import serializers
from rest_framework_json_api.relations import ResourceRelatedField
from ..models.subscription import Subscription
from ..models.book import Book
from ..models.subscriber import Subscriber
from .subscriber import SubscriberSerializer
from .book import BookSerializer

class SubscriptionSerializer(serializers.ModelSerializer):

        included_serializers = {
                'subscriber': SubscriberSerializer,
                'book': BookSerializer
        }

        # book =  serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), required=True)

        class Meta:
                model = Subscription
                fields = ('subscriber', 'book', 'borrowed_date', 'amount_paid', 'days', 'returned', 'due_amount')



