from django.shortcuts import render
from rest_framework import viewsets

from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework.filters import SearchFilter

from ..serializers.author import AuthorSerializer
from ..models.author import Author

# Create your views here.

class AuthorViewSet(viewsets.ModelViewSet):
        queryset = Author.objects.all()
        serializer_class = AuthorSerializer

        filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
                      django_filters.DjangoFilterBackend, SearchFilter)

        filterset_fields = {
                'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                # 'name': ( 'iexact', 'contains'),
                # 'address': ( 'iexact', 'contains'),
        }

        ordering_fields = ('id', 'name')
        search_fields = ('id', 'name', 'address' )
        