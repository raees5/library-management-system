from django.shortcuts import render
from rest_framework import viewsets

from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework.filters import SearchFilter
from ..serializers.book import BookSerializer
from ..models.book import Book

# Create your views here.

class BookViewSet(viewsets.ModelViewSet):
        queryset = Book.objects.all()
        serializer_class = BookSerializer

        filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
                      django_filters.DjangoFilterBackend, SearchFilter)

        filterset_fields = {
                'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                'count': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                'subscription_cost': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                # 'description': ( 'iexact', 'contains'),
                # 'topic': ( 'iexact', 'contains'),
        }

        ordering_fields = ('id', 'topic')
        search_fields = ('id', 'topic', 'description' )
        