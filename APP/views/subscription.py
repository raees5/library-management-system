from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework.filters import SearchFilter
from rest_framework import status

from ..serializers.subscription import SubscriptionSerializer
from ..models.subscription import Subscription

from datetime import datetime, timedelta
import json
# Create your views here.

class SubscriptionViewSet(viewsets.ModelViewSet):
        queryset = Subscription.objects.all()
        serializer_class = SubscriptionSerializer


        filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
                      django_filters.DjangoFilterBackend, SearchFilter)

        filterset_fields = {
                # 'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
        }

        ordering_fields = ('id', 'book')
        search_fields = ('id', 'amount_paid', 'returned' )

