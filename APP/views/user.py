from django.shortcuts import render
from rest_framework import viewsets

from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework.filters import SearchFilter

from ..serializers.user import UserSerializer
from django.contrib.auth.models import User

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
        queryset = User.objects.all()
        serializer_class = UserSerializer

        filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
                      django_filters.DjangoFilterBackend, SearchFilter)

        filterset_fields = {
                'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                # 'username': ( 'iexact', 'contains', 'icontains'),
                # 'email': ( 'iexact', 'contains', 'icontains'),
        }

        ordering_fields = ('id', 'username')
        search_fields = ('id', 'username', 'email' )
        