from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_json_api import filters

from ..models.subscription import Subscription

from datetime import datetime, timedelta
import json
# Create your views here.


class ListSubscriptionView(APIView):
        def get(self, request, format=None):
                """
                        Return a list of all users.
                """
                subscribers = Subscription.objects.filter(returned=False)

                person = list()
                for subscriber in subscribers:
                        data = dict()
                        date = datetime.strptime(str(subscriber.borrowed_date), '%Y-%d-%m')
                        expiry = date + timedelta(subscriber.days)
                        data['id'] = subscriber.id
                        data['subscriber'] = str(subscriber.subscriber.id)
                        data['expiry'] = expiry
                        person.append(data)

                # serializer = SubscriptionSerializer(subscribers, many=True)
                # return Response(serializer.data)
                return Response({'data': person})