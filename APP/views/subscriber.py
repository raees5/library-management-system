from django.shortcuts import render
from rest_framework import viewsets

from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework.filters import SearchFilter
from ..serializers.subscriber import SubscriberSerializer
from ..models.subscriber import Subscriber

# Create your views here.

class SubscriberViewSet(viewsets.ModelViewSet):
        queryset = Subscriber.objects.all()
        serializer_class = SubscriberSerializer


        filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
                      django_filters.DjangoFilterBackend, SearchFilter)

        filterset_fields = {
                'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                'user': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
                # 'address': ( 'iexact', 'contains'),
                # 'phone': ( 'iexact', 'contains'),
        }

        ordering_fields = ('id', 'phone')
        search_fields = ('id', 'phone', 'address' )



