from django.db import models
from .subscriber import Subscriber
from .book import Book

class Subscription(models.Model):
        subscriber = models.ForeignKey(Subscriber, related_name='users', on_delete=models.CASCADE)
        book = models.ForeignKey(Book, on_delete=models.CASCADE)
        borrowed_date = models.DateField(auto_now_add=True)
        amount_paid = models.PositiveIntegerField()
        days = models.PositiveIntegerField()
        returned = models.BooleanField()
        due_amount = models.PositiveIntegerField(null=True, blank=True)

        def __str__(self):
                return str(self.subscriber)