# Generated by Django 3.1 on 2020-08-10 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('APP', '0004_remove_subscription_due_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='due_amount',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
